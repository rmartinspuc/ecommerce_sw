﻿namespace INFRA.Ecommerce_SW.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Produto", name: "Promocao_IdPromocao", newName: "IdPromocao");
            RenameIndex(table: "dbo.Produto", name: "IX_Promocao_IdPromocao", newName: "IX_IdPromocao");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Produto", name: "IX_IdPromocao", newName: "IX_Promocao_IdPromocao");
            RenameColumn(table: "dbo.Produto", name: "IdPromocao", newName: "Promocao_IdPromocao");
        }
    }
}
