﻿using DOMAIN.Ecommerce_SW.Entidades;
using DOMAIN.Ecommerce_SW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INFRA.Ecommerce_SW.Repositorios
{
    public class PromocaoRepository : RepositoryBase<Promocao>, IPromocaoRepository
    {
    }
}
