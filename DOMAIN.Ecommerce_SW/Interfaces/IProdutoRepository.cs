﻿using DOMAIN.Ecommerce_SW.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOMAIN.Ecommerce_SW.Interfaces
{
    public interface IProdutoRepository : IRepositoryBase<Produto>
    {
    }
}
