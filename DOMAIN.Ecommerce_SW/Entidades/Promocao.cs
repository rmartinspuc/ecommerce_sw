﻿using System.Collections.Generic;

namespace DOMAIN.Ecommerce_SW.Entidades
{
    public partial class Promocao
    {
        public int IdPromocao { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public ICollection<Produto> Produtos { get; set; }

    }
}