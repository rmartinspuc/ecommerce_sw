﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOMAIN.Ecommerce_SW.Entidades
{
    public partial class Produto
    {
        public int IdProduto { get; set; }

        
        public string Nome { get; set; }
        public Double Preco { get; set; }

        [ForeignKey("Promocao")]
        public int? IdPromocao { get; set; }
        public Promocao Promocao { get; set; }

    }
}
