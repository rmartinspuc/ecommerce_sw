﻿using DOMAIN.Ecommerce_SW.Entidades;
using DOMAIN.Ecommerce_SW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOMAIN.Ecommerce_SW.Servicos
{
    public class PromocaoService : ServiceBase<Promocao>, IPromocaoService
    {
        private readonly IPromocaoRepository _promocaoRepository;
        public PromocaoService(IPromocaoRepository promocaoRepository) : base(promocaoRepository)
        {
            _promocaoRepository = promocaoRepository;
        }
    }
}
