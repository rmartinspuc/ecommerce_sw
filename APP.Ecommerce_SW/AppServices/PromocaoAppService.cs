﻿using APP.Ecommerce_SW.Interfaces;
using DOMAIN.Ecommerce_SW.Entidades;
using DOMAIN.Ecommerce_SW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.Ecommerce_SW.AppServices
{
    public class PromocaoAppService : AppServiceBase<Promocao>, IPromocaoAppService
    {
        public PromocaoAppService(IPromocaoService serviceBase) : base(serviceBase)
        {
        }
    }
}
