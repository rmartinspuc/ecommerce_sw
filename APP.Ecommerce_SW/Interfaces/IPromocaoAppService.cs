﻿using DOMAIN.Ecommerce_SW.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APP.Ecommerce_SW.Interfaces
{
    public interface IPromocaoAppService : IAppServiceBase<Promocao>
    {
    }
}
