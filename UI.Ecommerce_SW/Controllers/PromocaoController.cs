﻿using APP.Ecommerce_SW.Interfaces;
using DOMAIN.Ecommerce_SW.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Ecommerce_SW.Models;

namespace UI.Ecommerce_SW.Controllers
{
    public class PromocaoController : Controller
    {
        private readonly IPromocaoAppService _promocaoApp;

        public PromocaoController(IPromocaoAppService promocaoApp)
        {
            _promocaoApp = promocaoApp;
        }

        // GET: Promocao
        public ActionResult Index()
        {
            var listaPromocoes = _promocaoApp.GetAll();

            if (listaPromocoes.Any())
            {
                return View(Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<IEnumerable<PromocaoModel>>(listaPromocoes));
            }
            else
            {
                return View(new List<PromocaoModel>());
            }

        }


        // GET: Promocao/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Promocao/Create
        [HttpPost]
        public ActionResult Create(PromocaoModel promocao)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var promocaoDomain = Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<Promocao>(promocao);
                    _promocaoApp.Add(promocaoDomain);

                    return RedirectToAction("Index");
                }

                return View(promocao);
            }
            catch
            {
                return View();
            }
        }




        // GET: Promocao/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Promocao/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var promocao = _promocaoApp.GetById(id);
                _promocaoApp.Remove(promocao);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
