﻿using APP.Ecommerce_SW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Ecommerce_SW.Models;

namespace UI.Ecommerce_SW.Controllers
{
    public class HomeController : Controller
    {
        private readonly IProdutoAppService _produtoApp;

        public HomeController(IProdutoAppService produtoApp)
        {
            _produtoApp = produtoApp;
        }
        public ActionResult Index()
        {
            var listaProdutos = _produtoApp.GetAll();

            if (listaProdutos.Any())
            {
                return View(Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<IEnumerable<ProdutoModel>>(listaProdutos));
            }
            else
            {
                return View(new List<ProdutoModel>());
            }
        }

    }
}