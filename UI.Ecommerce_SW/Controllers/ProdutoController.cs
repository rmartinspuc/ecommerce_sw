﻿using APP.Ecommerce_SW.Interfaces;
using AutoMapper;
using DOMAIN.Ecommerce_SW.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Ecommerce_SW.Models;
using UI.Ecommerce_SW.ViewModels;

namespace UI.Ecommerce_SW.Controllers
{
    public class ProdutoController : Controller
    {
        private readonly IProdutoAppService _produtoApp;
        private readonly IPromocaoAppService _promocaoApp;

        public Dictionary<int, int> ListaCarrinho { get; private set; }

        public ProdutoController(IProdutoAppService produtoApp, IPromocaoAppService promocaoApp)
        {
            _produtoApp = produtoApp;
            _promocaoApp = promocaoApp;
            ListaCarrinho = new Dictionary<int, int>();
        }
        // GET: Produto
        public ActionResult Index()
        {

            var listaProdutos = _produtoApp.GetAll();

            if (listaProdutos.Any())
            {
                return View(Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<IEnumerable<ProdutoModel>>(listaProdutos));
            }
            else
            {
                return View(new List<ProdutoModel>());
            }

           
        }

        // GET: Produto/Details/5
        public ActionResult Details(int id)
        {
            var produto = _produtoApp.GetById(id);
            var produtoModel = Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<ProdutoModel>(produto);

            return View(produtoModel);
        }

        // GET: Produto/Create
        public ActionResult Create()
        {
            ViewBag.Promocoes = new SelectList(_promocaoApp.GetAll(), "IdPromocao", "Nome");
            return View();
        }

        // POST: Produto/Create
        [HttpPost]
        public ActionResult Create(ProdutoModel produto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var produtoDomain = Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<Produto>(produto);
                    _produtoApp.Add(produtoDomain);

                    return RedirectToAction("Index");
                }

                ViewBag.Promocoes = new SelectList(_promocaoApp.GetAll(), "IdPromocao", "Nome");
                return View(produto);
            }
            catch
            {
                return View();
            }
        }

        // GET: Produto/Edit/5
        public ActionResult Edit(int id)
        {
            var produto = _produtoApp.GetById(id);
            var produtoModel = Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<ProdutoModel>(produto);

            ViewBag.Promocoes = new SelectList(_promocaoApp.GetAll(), "IdPromocao", "Nome",  produto.Promocao.IdPromocao);

            return View(produtoModel);
        }

        // POST: Produto/Edit/5
        [HttpPost]
        public ActionResult Edit(ProdutoModel produto)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var produtoDomain = Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<Produto>(produto);
                    _produtoApp.Update(produtoDomain);

                    return RedirectToAction("Index");
                }

                ViewBag.Promocoes = new SelectList(_promocaoApp.GetAll(), "IdPromocao", "Nome", produto.Promocao.IdPromocao);
                return View(produto);
            }
            catch
            {
                return View();
            }
        }

        // GET: Produto/Delete/5
        public ActionResult Delete(int id)
        {
            var produto = _produtoApp.GetById(id);
            var produtoModel = Mapeamentos.AutoMapperConfig.Mapper.MapperConfig.Map<ProdutoModel>(produto);

            return View(produtoModel);
        }

        // POST: Produto/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                var produto = _produtoApp.GetById(id);
                _produtoApp.Remove(produto);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public void AdicionarAoCarrinho(int id, int quantidade)
        {
            ListaCarrinho = Session["Carrinho"] as Dictionary<int, int>;

            if(ListaCarrinho == null)
            {
                ListaCarrinho = new Dictionary<int, int>();
            }

            if (ListaCarrinho.ContainsKey(id))
            {
                int novaQuantidade = ListaCarrinho[id] + quantidade;
                ListaCarrinho.Remove(id);
                ListaCarrinho.Add(id, novaQuantidade);

            }
            else
            {
                ListaCarrinho.Add(id, quantidade);
            }
            

            Session["Carrinho"] = ListaCarrinho;
        }

        public ActionResult Carrinho()
        {
            var lista = Session["Carrinho"] as Dictionary<int, int>;
            List<CarrinhoViewModel> listaCarrinhoViewModel = new List<CarrinhoViewModel>();
            if(lista != null)
            {
                foreach (var item in lista)
                {
                    var produto = _produtoApp.GetById(item.Key);
                    listaCarrinhoViewModel.Add(new CarrinhoViewModel()
                    {
                        Item = produto.Nome,
                        Preco = produto.Preco * item.Value,//CalcularPrecoCarrinho(produto, item), //TODO - TRECHO REMOVIDO DEVIDO A FALHA NA CONFIGURAÇÃO E CRIAÇÃO DA BASE DE DADOS.
                        Quantidade = item.Value,
                        Promocao = produto.Promocao != null ? produto.Promocao.Nome : "-"
                    });
                }
            }
            
            return View(listaCarrinhoViewModel);
        }

        /// <summary>
        /// CAlcula preços de acordo com promoção cadastrada
        /// </summary>
        /// <param name="produto"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private double CalcularPrecoCarrinho(Produto produto, KeyValuePair<int, int> item)
        {
            switch (produto.Promocao.Nome.ToLower())
            {
                case "pague 1 leve 2": 
                    if(item.Value % 2 == 0)
                    {
                        return (produto.Preco * item.Value) / 2;
                    }
                    else
                    {
                        return ((produto.Preco * item.Value) / 2) + produto.Preco;
                    }
                case "3 por 10 reais":
                    if(item.Value % 3 == 0)
                    {
                        return 10 * (item.Value / 3);
                    }
                    else
                    {
                        return produto.Preco * item.Value;
                    }
                default:
                    return produto.Preco * item.Value;
            }
        }
    }
}
