﻿using AutoMapper;
using DOMAIN.Ecommerce_SW.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.Ecommerce_SW.Models;

namespace UI.Ecommerce_SW.Mapeamentos
{
    public class ProdutoProfile : Profile
    {
        public ProdutoProfile()
        {
            CreateMap<Produto, ProdutoModel>()
                .ForMember(p => p.IdPromocao, opt => opt.Ignore());

            CreateMap<ProdutoModel, Produto>()
                .ForMember(p => p.Promocao, opt => opt.MapFrom(src => src.Promocao));
        }
    }
}