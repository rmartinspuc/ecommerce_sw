﻿using AutoMapper;
using DOMAIN.Ecommerce_SW.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.Ecommerce_SW.Models;

namespace UI.Ecommerce_SW.Mapeamentos
{
    public class PromocaoProfile : Profile
    {
        public PromocaoProfile()
        {
            CreateMap<Promocao, PromocaoModel>();

            CreateMap<PromocaoModel, Promocao>()
                .ForMember(p => p.Produtos, opt => opt.Ignore());
        }
    }
}