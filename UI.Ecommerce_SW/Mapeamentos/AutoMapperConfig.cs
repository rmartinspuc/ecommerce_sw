﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Ecommerce_SW.Mapeamentos
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            var profiles = (from type in typeof(AutoMapperConfig).Assembly.GetTypes()
                            where typeof(Profile).IsAssignableFrom(type) && !type.IsAbstract && type.GetConstructor(Type.EmptyTypes) != null
                            select type).Select(d => (Profile)Activator.CreateInstance(d)).ToArray();

            var config = new MapperConfiguration(cnfg =>
            {
                foreach (var profile in profiles)
                {
                    cnfg.AddProfile(profile);
                }
            });
            Mapper.MapperConfig = config.CreateMapper();

            config.AssertConfigurationIsValid();
        }

        public static class Mapper
        {
            public static IMapper MapperConfig;
        }
    }
}