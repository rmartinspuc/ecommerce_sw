﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Ecommerce_SW.ViewModels
{
    public class CarrinhoViewModel
    {
        public String Item { get; set; }
        public int Quantidade { get; set; }
        public Double Preco { get; set; }
        public String Promocao { get; set; }
    }
}