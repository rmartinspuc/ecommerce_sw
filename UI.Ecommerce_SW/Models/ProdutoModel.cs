﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Ecommerce_SW.Models
{
    public class ProdutoModel
    {
        [Key]
        public int IdProduto { get; set; }

        public int IdPromocao { get; set; }

        [Required(ErrorMessage = "Preencha o campo Nome")]
        [MaxLength(250, ErrorMessage = "Máximo {0} caracteres")]
        public string Nome { get; set; }

        [DataType(DataType.Currency)]
        [Range(typeof(double), "0", "999999999999")]
        [Required(ErrorMessage = "Preencha um valor")]
        [Display(Name = "Preço")]
        public Double Preco { get; set; }

        public PromocaoModel Promocao { get; set; }
    }
}