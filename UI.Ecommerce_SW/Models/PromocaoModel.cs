﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Ecommerce_SW.Models
{
    public class PromocaoModel
    {
        [Key]
        public int IdPromocao { get; set; }

        [Display(Name = "Promoção")]
        public string Nome { get; set; }

        [Display(Name = "Descrição")]
        public string Descricao { get; set; }
    }
}